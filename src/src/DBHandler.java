package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A class that handles the database (usernames and messages).
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class DBHandler {

	private Connection connection = null;
	private String url = "jdbc:mysql://localhost:3306/";
	private String driver = "com.mysql.jdbc.Driver";
	private String db = "p2p";
	private String username = "root";
	private String password = "KfCa7RkQ3cQxe84brKyz0y34FEIe3RubGtqlU3kX43ACRF5IvV5FyVHYig4K69gAAFxtEztOSbS2QBQ0GNdAPPzL90iHVcftgJBw";

	/**
	 * Method to connect to db. Credentials is already filled in above.
	 */
	private void connect() {
		// Tries to establish a connection to the database
		System.out.println("Trying to connect");
		try {
			// Loads the injection driver with url, driver, username and password.
			Class.forName(driver);
			connection = DriverManager.getConnection(url + db, username, password);
			System.out.println("Connected!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes new user to db.
	 * 
	 * @param username - String to be written to db.
	 */
	@SuppressWarnings("unused")
	public void writeNewUser(String username) throws SQLException {
		connect();
		int UID = username.hashCode();
		Statement statement = connection.createStatement();

		try {
			String addNewEntry = "INSERT INTO `p2p`.`users` (`ID`, `alias`) VALUES ('" + UID + "', '" + username + "');";
			int executeUserInstertion = statement.executeUpdate(addNewEntry); // Executes the INSERT-statement.
			System.out.println("Added user: " + username); //DEBUG
			connection.close();
			System.out.println("db connection closed");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Writes new row to db.
	 * 
	 * @param message - String to be written to db.
	 */
	@SuppressWarnings("unused")
	public void writeMessage(String sender, String receiver, String message) throws SQLException {
		connect();
		try {
			Statement statement = connection.createStatement();
			String addNewEntry = "INSERT INTO `p2p`.`conversation` (`sender`, `receiver`, `message`, `unread`, `sent`, `time`) VALUES ('" + sender + "', '" + receiver + "', '" + message + "', '1', '2', CURRENT_TIMESTAMP)";
			int executeMessageInsertion = statement.executeUpdate(addNewEntry); // Executes the INSERT-statement.
			System.out.println("Added message: " + message + ", from " + sender + " to " + receiver);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		connection.close();
	}

	/**
	 * Method to fetch unread messages from server and mark them as read.
	 * 
	 * @param sender - the senders name
	 * @param receiver - the receivers name
	 * @param row - X row to get (will properly not use this)
	 * @return messages - a String[] with all unread messages
	 */
	public String getMessages(String sender, String receiver, int row) throws SQLException {
		String messages = "";
		ResultSet rs = null;
		PreparedStatement getMessage = null;
		PreparedStatement markAsRead = null;
		connect();
		try {
			getMessage = connection.prepareStatement("SELECT *FROM `conversation`WHERE `sender` LIKE '"+receiver+"'AND `receiver` LIKE '"+sender+"'AND `unread` =1 OR `sender` LIKE '"+sender+"'AND `receiver` LIKE '"+receiver+"'AND `sent` = 2 ORDER BY `conversation`.`time` ASC");
			System.out.println("Trying to execute query...");
			rs = getMessage.executeQuery();
			System.out.println("Query executed!");
			while (rs.next()) {
				System.out.println("Trying to read query...");
				System.out.println("Sender=" + rs.getString(1));
				System.out.println("Receiver=" + rs.getString(2));
				System.out.println("Message=" + rs.getString(3));
				System.out.println("Status=" + rs.getInt(4));
				System.out.println("Status=" + rs.getInt(5));
				System.out.println("Timestamp=" + rs.getTimestamp(6));
				if (sender.equals(rs.getString(1)) && (rs.getInt(5) == 2)) {
					markAsSent(rs.getString(1), rs.getString(2), rs.getString(3));
				}else {
					markAsReceived(rs.getString(1), rs.getString(2), rs.getString(3));	
				}
				messages += (rs.getString(1) + "#" + rs.getString(2) + "#" + rs.getString(3) + "#" + rs.getString(4) + "#" + rs.getString(5) + "#" + rs.getString(6) + "%");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("[db]messages = " + messages);
		return messages;
	}

	/**
	 * Method to fetch unread messages from server and mark them as read.
	 * 
	 * @param sender - the senders name
	 * @param receiver - the receivers name
	 * @param row - X row to get (will properly not use this)
	 * @return messages - a String[] with all unread messages
	 */
	public String getAllMessages(String sender, String receiver, int row) throws SQLException {
		String messages = "";
		ResultSet rs = null;
		PreparedStatement getMessage = null;
		PreparedStatement markAsRead = null;
		connect();
		try {
			System.out.println("RUNNING GET ALL MESSAGES");
			getMessage = connection.prepareStatement("SELECT *FROM `conversation`WHERE `sender` LIKE '"+receiver+"'AND `receiver` LIKE '"+sender+"'AND `unread` =0 OR `sender` LIKE '"+sender+"'AND `receiver` LIKE '"+receiver+"'AND `unread` =0 ORDER BY `conversation`.`time` ASC");
			System.out.println("Trying to execute query...");
			rs = getMessage.executeQuery();
			System.out.println("Query executed!");
			while (rs.next()) {
				System.out.println("Trying to read query...");
				System.out.println("Sender=" + rs.getString(1));
				System.out.println("Receiver=" + rs.getString(2));
				System.out.println("Message=" + rs.getString(3));
				System.out.println("Status=" + rs.getInt(4));
				System.out.println("Status=" + rs.getInt(5));
				System.out.println("Timestamp=" + rs.getTimestamp(6));
				messages += (rs.getString(1) + "#" + rs.getString(2) + "#" + rs.getString(3) + "#" + rs.getString(4) + "#" + rs.getString(5) + "#" + rs.getString(6) + "%");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("[db ALL]messages = " + messages);
		return messages;
	}

	/**
	 * SET `sent` = '0'
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 */
	public void markAsSent(String sender, String receiver, String message) {
		System.out.println("Marking as sent: \n\tSender: " + sender + "\n\tReceiver: " + receiver + "\n\tMessage:" + message);
		PreparedStatement markAsRead = null;
		try {
			markAsRead = connection.prepareCall("UPDATE `p2p`.`conversation` SET `sent` = '0' WHERE `conversation`.`message` = '" + message + "' AND `conversation`.`receiver` = '" + receiver + "' AND `conversation`.`sender` = '" + sender + "';");
			markAsRead.executeUpdate(); // BETA
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * SET `unread` = '0'
	 * 
	 * @param sender
	 * @param receiver
	 * @param message
	 */
	public void markAsReceived(String sender, String receiver, String message) {
		PreparedStatement markAsRead = null;
		System.out.println("Marking as read: \nSender: " + sender + "\nReceiver: " + receiver + "\nMessage:" + message);
		try {
			markAsRead = connection.prepareCall("UPDATE `p2p`.`conversation` SET `unread` = '0' WHERE `conversation`.`message` = '" + message + "' AND `conversation`.`receiver` = '" + receiver + "' AND `conversation`.`sender` = '" + sender + "';");
			markAsRead.executeUpdate(); // BETA
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}