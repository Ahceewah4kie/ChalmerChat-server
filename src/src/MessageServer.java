package src;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MessageServer {
	private String query;
	private String[] queryArray;
	private int port = 4949;

	public MessageServer() {
		Thread connectThread = new Thread(new NewConnection());
		connectThread.start();
	}

	private class NewConnection implements Runnable {
		public void run() {
			Socket socket;
			try {
				ServerSocket serverSocket = new ServerSocket(port); // TODO: Fix possible leak!
				while (true) { // TODO: serverSocket.bound()?
					socket = serverSocket.accept();
					new Thread(new TalkToClient(socket)).start();
					System.out.println(socket.getInetAddress().getHostAddress() + " connected");
				}
//				serverSocket.close(); //TODO:BETA
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private class TalkToClient implements Runnable {
		private String[] badWords = { "DELETE", "INSERT", "DROP", "SELECT", "UPDATE" };
		private String answer;
		private Socket socket;

		public TalkToClient(Socket socket) {
			this.socket = socket;
		}

		public void run() {
			DBHandler db = new DBHandler();
			try {
				// Receive query
				String sender, receiver, message;
				ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
				ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());

				query = input.readUTF();
				System.out.println("Received: " + query);
				queryArray = query.split(",");
				sender = queryArray[0];
				receiver = queryArray[1];
				message = queryArray[2];

				// This checks if the message is real, or a query for unread messages.
				if (queryArray[2].toString().equals("@4-8-15-16-23-42-LOST")) {
					String messagesSent = db.getMessages(sender, receiver, -1);
					System.out.println("[tcp]messages = " + messagesSent);
					output.writeUTF(messagesSent);
					output.flush();
				} else if (queryArray[2].toString().equals("@4-8-15-16-23-42")) {
					String messagesSent = db.getAllMessages(sender, receiver, -1);
					System.out.println("[tcp ALL]messages = " + messagesSent);
					output.writeUTF(messagesSent);
					output.flush();
				}else {
					System.out.println("WRITING NEW MESSAGE - ELSE RUNS");
					db.writeMessage(queryArray[0], queryArray[1], queryArray[2]);
					output.writeUTF(queryArray[0] + " wrote " + queryArray[2] + " to" + queryArray[1]);
					output.flush();
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error with: " + socket.getInetAddress().toString());
			}

			// Close connection
			try {
				socket.close();
				System.out.println(socket.getInetAddress().getHostAddress() + " disconnected");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error terminating connection with " + socket.getInetAddress().toString());
			}
		}

	}
}
